#!/usr/bin/env python3
'''
Adapted from a Perl script written by Richard Kemp, this attempts to model the
loss of ENU induced, tumour promoting mutations on an hourly basis
'''
import sys
import random


# Variables from command line
sc_per_crypt = int(sys.argv[1])
rr = float(sys.argv[2])
run_number = int(sys.argv[3])
num_enu_muts = int(sys.argv[4])

# Static vars
num_crypts = 100000
enu_hour = 525600 # Hour of ENU pulse
replacement_prob = 1/(24/(rr * sc_per_crypt))
day = 0
end_hour = 790000
#mut_freq_cutoff = 0.001
mut_freq_cutoff = 0.0001141553

all_crypts = [[0 for _ in range(0, sc_per_crypt)] for _ in range(num_crypts)]
# Init new SystemRandom
rnd = random.SystemRandom()

# Headers for output
print(f'SC per crypt={sc_per_crypt}\tReplace rate={rr}\tDay of ENU={enu_hour}\tENU Mutations={num_enu_muts}')
print(f'SC\tRR\tRun\tHour\tDay\tYear\tWT\tPartial\tMonoclonal\tMarked')

for hour in range(end_hour):
    # Initialise a new count every hour of wt/mc and partials
    wt = 0
    mc = 0
    partial = 0
    # Pick a properly random mutation frequency
    mut_freq = rnd.random()
    # Updated mutation frequency to be a tuneable variable
    if mut_freq < mut_freq_cutoff:
#        print('Mutation occurred!')
        # Picks a random crypt in the all_crypts list, then picks a random
        # stem cell and mutates it, then saves it back into the list
        c_index = random.randint(0, len(all_crypts) - 1)
        sc_index = random.randint(0, sc_per_crypt - 1)
        all_crypts[c_index][sc_index] = 1
    # Pulse of ENU mutations on enu_hour
    if hour == enu_hour:
        for _ in range(num_enu_muts):
            c_index = random.randint(0, len(all_crypts) - 1)
            sc_index = random.randint(0, sc_per_crypt - 1)
            all_crypts[c_index][sc_index] = 1
    # Pop a random stem cell (marked or not) out of a crypt
    for this_crypt in all_crypts:
        # If a crypt is wholly WT or wholly populate, division has no effect and
        # so we can skip it
        if 0 in this_crypt:
            if 1 in this_crypt:
                partial += 1
                # Likelihood of each crypt undergoin division and replacement
                replace_freq = rnd.random()
                if replace_freq < replacement_prob:
                    # pick a random cell - if it is 1, add a 1, if not add a 0
                    sc_add_choice = random.randint(0, sc_per_crypt - 1)
                    if this_crypt[sc_add_choice] == 1:
                        this_crypt.append(1)
                    elif this_crypt[sc_add_choice] == 0:
                        this_crypt.append(0)
                    # pick another random cell and remove it
                    sc_remove_choice = random.randint(0, sc_per_crypt - 1)
                    del this_crypt[sc_remove_choice]
                # minor style point as a mutation could have pushed this particular
                # crypt into wt or mc status so we should probably double check at
                # this point whether this is the case, maybe a point of discussion
            else:
                wt += 1
        else:
            mc += 1
    day = hour//24
    year = day//365
    wt_freq = wt/num_crypts
    mc_freq = mc/num_crypts
    partial_freq = partial/num_crypts
    marked_crypts = partial_freq + mc_freq
    # Print out the results once a day
    if (hour % 24) == 0:
        print(f'{sc_per_crypt}\t{rr}\t{run_number}\t{hour}\t{day}\t{year}\t{wt_freq}\t{partial_freq}\t{mc_freq}\t{marked_crypts}')
#        print(f'{wt}\t{mc}\t{partial}')
