#!/usr/bin/env python3
'''
Second attempt using dicts to crudely simulate 1d space for both crypts and stem
cells, with nearest neighbour updates
'''
from concurrent.futures import ProcessPoolExecutor
import random
import sys
import csv

def mutate(key, crypt_sets):
    '''
    Mutates a stem cell in a crypt - the crypt is now definitely at least a
    partial, but could also be a monoclonal so we add it's key to the
    corresponding list
    '''
    if key in crypt_sets[1]:
        crypt_sets[1].remove(key)
    if key in crypt_sets[3]:
        return
    sc_index = random.randint(0, len(crypt_sets[0][0]) - 1)
    crypt_sets[0][key][sc_index] = 1

def divide(key, crypt_sets):
    '''
    Picks a cell and causes it to divide, replacing the cell to its left with a
    copy of itself. Never picks the first element.
    '''
    all_crypts = crypt_sets[0]
    sc_per_crypt = len(crypt_sets[0][0])
    sc_divide_choice = random.randint(1, sc_per_crypt - 1)
    div = all_crypts[key][sc_divide_choice]
    all_crypts[key][sc_divide_choice - 1] = div
### Test nearest neighbour, coin flip on whether it is left or right
##    crypt_len = len(crypt_sets[0][0]) - 1
##    sc_divide_choice = random.randint(0, crypt_len)
##    div = crypt_sets[0][key][sc_divide_choice]
##    if not div:
##        if random.random() > 0.72:
##            return
##    if bool(random.getrandbits(1)):
##        sc_flip = sc_divide_choice + 1
##    else:
##        sc_flip = sc_divide_choice - 1
##    # Wrap around logic
##    if sc_flip < 0:
##        sc_flip = crypt_len
##    elif sc_flip >= crypt_len:
##        sc_flip = 0
##    crypt_sets[0][key][sc_flip] = div

def check(key, crypt_sets):
    '''
    Checks the status of a crypt, and adds it to the relevant list
    '''
    if 1 in crypt_sets[0][key].values():
        if 0 in crypt_sets[0][key].values():
            if key in crypt_sets[1]:
                crypt_sets[1].remove(key)
            elif key in crypt_sets[3]:
                crypt_sets[3].remove(key)
            crypt_sets[2].add(key)
        else:
            if key in crypt_sets[1]:
                crypt_sets[1].remove(key)
            elif key in crypt_sets[2]:
                crypt_sets[2].remove(key)
            crypt_sets[3].add(key)
    else:
        if key in crypt_sets[2]:
            crypt_sets[2].remove(key)
        elif key in crypt_sets[3]:
            crypt_sets[3].remove(key)
        crypt_sets[1].add(key)

def process_run(run_number, sc_per_crypt, rr, num_pulse_muts, num_crypts, \
                pulse_hour, replacement_prob, end_hour, mut_freq_cutoff):
    '''
    Does a run
    '''
    day = 0
    #Init new SystemRandom
    rnd = random.SystemRandom()

    # Init crypt dicts
    all_crypts = {}
    for crypt in range(num_crypts):
        this_crypt = {}
        for sc in range(sc_per_crypt):
            this_crypt[sc] = 0
        all_crypts[crypt] = this_crypt
    wt_crypts = set(all_crypts.keys())
    partial_crypts = set()
    mc_crypts = set()
    crypt_sets = (all_crypts, wt_crypts, partial_crypts, mc_crypts)

    with open(f'output_{num_pulse_muts}_{run_number}.out', 'w', newline='') as csvfile:
        csvwriter = csv.writer(csvfile, delimiter='\t')
        csvwriter.writerow([f'SC per crypt={sc_per_crypt}', f'Replace rate={rr}', \
                            f'Day of pulse={pulse_hour}', \
                            f'Pulse Mutations={num_pulse_muts}'])
        csvwriter.writerow(['SC', 'RR', 'Run', 'Hour', 'Day', 'Year', 'WT', \
                            'Partial', 'Monoclonal', 'Marked'])
        for hour in range(end_hour):
            mut_freq = rnd.random()
            if mut_freq < mut_freq_cutoff:
                c_index = random.randint(0, num_crypts - 1)
                mutate(c_index, crypt_sets)
                check(c_index, crypt_sets)
            if hour == pulse_hour:
                for _ in range(num_pulse_muts):
                    c_index = random.randint(0, num_crypts - 1)
                    mutate(c_index, crypt_sets)
                    check(c_index, crypt_sets)
            for p_crypt in partial_crypts.copy():
                replace_freq = rnd.random()
                if replace_freq < replacement_prob:
                    divide(p_crypt, crypt_sets)
                    check(p_crypt, crypt_sets)
            day = hour//24
            year = day//365
            wt_freq = len(wt_crypts)/num_crypts
            mc_freq = len(mc_crypts)/num_crypts
            partial_freq = len(partial_crypts)/num_crypts
            marked_crypts = partial_freq + mc_freq
            if (hour % 24) == 0:
                csvwriter.writerow([sc_per_crypt, rr, run_number, hour, day, year, \
                                    wt_freq, partial_freq, mc_freq, marked_crypts])

def main():
    '''
    '''
    # Variables from command line
    sc_per_crypt = int(sys.argv[1])
    rr = float(sys.argv[2])
    # Trying multiprocessing
    run_count = int(sys.argv[3])
    num_pulse_muts = int(sys.argv[4])
    pulse_hour = int(sys.argv[5])/365/24
    replacement_prob = 1/(24/(rr * sc_per_crypt))

    # Static vars
    num_crypts = 100000
    end_hour = 790000
    #mut_freq_cutoff = 0.001
    mut_freq_cutoff = 0.0001141553

    with ProcessPoolExecutor() as executor:
        for run in range(1, run_count):
            executor.submit(process_run, run, sc_per_crypt, rr, num_pulse_muts, num_crypts, \
                            pulse_hour, replacement_prob, end_hour, mut_freq_cutoff)

if __name__ == '__main__':
    main()
