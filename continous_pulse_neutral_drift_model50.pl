#!/usr/bin/perl -w
# 
# Trying to model the loss of ENU induced tumour promoting mutations with time
# Converting to hourly basis

# $ARGV[0] = stem cells per crypt, $ARGV[1] = division/replacement per day, $ARGV[2] = run number
# e.g. for i in {1..5}; do perl DRIFT_MOUSE_MODEL_6.pl 6 0.2 $i; done

#### Some variable initialisation
$sc_per_crypt = $ARGV[0];    
$number_of_crypts = 100000;
$rr = $ARGV[1];   # division/replacement per day
$enu_day = 21900; # day of ENU pulse
$number_of_enu_muts = 50;
$replacement_prob = 1/(24/($rr * $sc_per_crypt));
$day = 0;

#### Headers for output
print "SC per crypt=$sc_per_crypt\tReplace rate=$rr\tDay of ENU=$enu_day\tENU mutations=$number_of_enu_muts\n";
print "SC\tRR\tRun\tHour\tDay\tYear\tWT\tPartial\tMonoclonal\tMarked\n";


$hour_daily_count = 0;

#### Initialising crypts, setting up the WT crypt field
$cells = 1;
while($cells <= $sc_per_crypt){
	push(@WT_CRYPT,0);
	push(@MC_CRYPT,1);
	$cells = $cells + 1;
}
$wt_crypt = join('',@WT_CRYPT);
$mc_crypt = join('',@MC_CRYPT);

$crypts = 1;
while($crypts <= $number_of_crypts){
	push(@ALL_CRYPTS,"$wt_crypt");
	$crypts = $crypts +1;
}


#### Mutate a random stem cell in a random crypt at a certain frequency per hour
$hour = 1;
while($hour <= 790000){

	$mut_freq = rand(1);
	if($mut_freq < 0.0001141553){					# currently one in 8760 chance every hour i.e. once per year on average (and not 1 in 24 chance every hour i.e. once per day on average as it is expected to be much less)
		$crypt_index = int(rand($number_of_crypts));
		@CRYPT = split('',$ALL_CRYPTS[$crypt_index]);

		$sc_index = int(rand($sc_per_crypt));
		$CRYPT[$sc_index] = "1";
	
		$ALL_CRYPTS[$crypt_index] = join('',@CRYPT);
	}
	
	
#### Pulse of ENU mutations on year 60

		if($day == $enu_day){
			while($number_of_enu_muts > 0){
				$crypt_index = int(rand($number_of_crypts));
				@CRYPT = split('',$ALL_CRYPTS[$crypt_index]);

				$sc_index = int(rand($sc_per_crypt));
				$CRYPT[$sc_index] = "1";
	
				$ALL_CRYPTS[$crypt_index] = join('',@CRYPT);
				$number_of_enu_muts = $number_of_enu_muts -1;
			}
		}
	
	
	
#### A random stem cell in a random crypt (marked or not) divides and a random stem cell is expelled from that crypt
#### Set up as neutral here - need to modify to incorporate bias - easiest way is to check the stem cell selected for expulsion first
#### and if it's not what you prefer to be expelled just choose another

	$crypt_index = 0;
	while($crypt_index < scalar@ALL_CRYPTS){
		if (($ALL_CRYPTS[$crypt_index] ne "$wt_crypt") && ($ALL_CRYPTS[$crypt_index] ne "$mc_crypt")){   # if crypt is WT or wholly populated do nothing since a division and replacement has no effect
	
			$replace_freq = rand(1);  #decide on the likelihood of each crypt undergoing a stem cell division and replacement
			if($replace_freq < $replacement_prob){			# small SI replacement rate is 0.1, stem cell number is 5, so one stem cell per crypt is replaced every other day
			
				@CRYPT = split('',$ALL_CRYPTS[$crypt_index]);
				$sc_index = int(rand($sc_per_crypt));
				if($CRYPT[$sc_index] eq "1"){
					push(@CRYPT,"1");
					$sc_index = int(rand($sc_per_crypt));
					splice(@CRYPT,$sc_index,1);
				}elsif($CRYPT[$sc_index] eq "0"){
					push(@CRYPT,"0");
					$sc_index = int(rand($sc_per_crypt));
					splice(@CRYPT,$sc_index,1);
				}	
				$ALL_CRYPTS[$crypt_index] = join('',@CRYPT);
			}
		}
		$crypt_index = $crypt_index +1;
	}	
	

#### Daily count up of WT, partial and mono-clonal crypt frequency

	$wt = 0;
	$mc = 0;
	$partial = 0;
	
	foreach $val (@ALL_CRYPTS){
		if($val eq "$wt_crypt"){
			$wt = $wt +1;
		}elsif($val eq "$mc_crypt"){
			$mc = $mc +1;
		}else{
			$partial = $partial +1;
		}
	}
	$day = $hour/24;
	$year = $day/365;
	$wt_freq = $wt / $number_of_crypts;
	$mc_freq = $mc / $number_of_crypts;
	$partial_freq = $partial / $number_of_crypts;
	$marked_crypts = $partial_freq + $mc_freq;
	
	$hour_daily_count = $hour_daily_count +1;
	if($hour_daily_count == 24){
		print "$ARGV[0]\t$ARGV[1]\t$ARGV[2]\t$hour\t$day\t$year\t$wt_freq\t$partial_freq\t$mc_freq\t$marked_crypts\n";
		$hour_daily_count = 0;
	}
	
	$hour = $hour +1;
}

exit;
